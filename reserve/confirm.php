<?php
$d = $_POST;
require_once('selectData.php');
require_once('checkData.php');
define('SMARTY_DIR','/var/www/vhosts/w7726.ws.domainking.cloud/takedive.com/smarty/libs/');
require_once(SMARTY_DIR.'Smarty.class.php');
$s = new Smarty();
$err = false;
$e = array();
if ($d['GO']!="") {
  if (($d['CELL']=="")&&($d['TEL']=="")) {
    $e += array("CELL"=>"電話番号が入力されていません。");
    $e += array("TEL"=>"携帯か固定電話どちらかを入力してください。");
    $err = true;
  }
  if ($d['EMAIL']=="") {
    $e += array("EMAIL"=>"メールアドレスが入力されていません。");
    $err = true;
  }
  if ($d['NAME']=="") {
    $e += array("NAME"=>"氏名が入力されていません。");
    $err = true;
  }
  if ($d['KANA']=="") {
    $e += array("KANA"=>"ふりがなが入力されていません。");
    $err = true;
  }

  $s = new Smarty();
  $p=1;
  if ($d['MEMBER2']!="") {
    $p++;
  }
  if ($d['MEMBER3']!="") {
    $p++;
  }
  if ($d['MEMBER4']!="") {
    $p++;
  }
  if ($d['MEMBER5']!="") {
    $p++;
  }
  $d['PERSON'] = $p;
  $s->assign("d",$d);
  if ($err) {
    $s->assign("remarks",$c_remarks);
    $s->assign("sexOpt",$a_sexOpt);
    $s->assign("styleOpt",$a_styleOpt);
    $s->assign("duringOpt",$a_duringOpt);
    $s->assign("skilOpt",$a_skilOpt);
    $s->assign("hotelOpt",$a_hotelOpt);
    $s->assign("bathOpt",$a_bathOpt);
    $s->assign("repeatOpt",$a_repeatOpt);
    $s->assign("rentallOpt",$a_rentallOpt);
    $s->assign("blankOpt",$a_blankOpt);
    $s->assign("yesnoOpt",$a_yesnoOpt);
    $s->assign("num123Opt",$a_num123Opt);
    $s->assign("e",$e);
    $s->display("input.tpl");
    exit;
  }
  $s->display("confirm.tpl");
  exit;
} else {
  echo "<pre>\n";
  print_r($d);
  echo "</pre>\n";
}

?>
