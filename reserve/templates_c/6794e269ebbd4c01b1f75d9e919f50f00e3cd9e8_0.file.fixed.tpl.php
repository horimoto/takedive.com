<?php
/* Smarty version 3.1.30, created on 2017-03-07 00:14:51
  from "/var/www/vhosts/takedive.com/httpdocs/reserve/templates/fixed.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58bd7ceb4fd596_98138899',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6794e269ebbd4c01b1f75d9e919f50f00e3cd9e8' => 
    array (
      0 => '/var/www/vhosts/takedive.com/httpdocs/reserve/templates/fixed.tpl',
      1 => 1488810468,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58bd7ceb4fd596_98138899 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
 <link rel="stylesheet" type="text/css" href="default.css">
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>TAKE DIVING/予約完了</title>
 <link rel="shortcut icon" href="../favicon.ico">
</head>
<body bgcolor="#66ffff">
<div align="center">
<IMG src="reserve.gif" width="268" height="135" alt="完了フォーム" border="0"></div>
<P align="center">以下のように予約を承りました。
ありがとうございます。気を付けていらっしゃってください。</p>

<HR width="100%" align="center">
<!-- <p>西表島ニラカナイへご宿泊で、ダイビングを申込んでいただこうとしているお客さまへ。</p>
<p>まず、こちらをごらんください。</p> -->

<div align="center">
<table border="0">
  <tr><th class="B" colspan="2">予約情報</th></tr>
  <tr><th>予約完了日時</th><td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['TODAY'];?>
</td></tr>
  <tr><th>予約番号</th><td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['RESNUM'];?>
 お問い合わせの際にお伝えください。</td></tr>
  <tr><th class="B" colspan="2">申込者(代表者)連絡先</th></tr>
  <tr><th>氏名</th><td><span class="rubi"><?php echo $_smarty_tpl->tpl_vars['d']->value['KANA'];?>
</span><br><?php echo $_smarty_tpl->tpl_vars['d']->value['NAME'];?>
</td></tr>
  <tr><th>性別年齢</th><td class="small1">性別：<?php echo $_smarty_tpl->tpl_vars['d']->value['SEX'];?>
&nbsp;&nbsp;年齢：<?php echo (($tmp = @$_smarty_tpl->tpl_vars['d']->value['AGE'])===null||$tmp==='' ? '？' : $tmp);?>
歳</td></tr>
  <tr><th>住所</th><td class="small1">〒<?php echo $_smarty_tpl->tpl_vars['d']->value['ZIP'];?>
 <?php echo $_smarty_tpl->tpl_vars['d']->value['ADDR'];?>
</td></tr>
  <tr><th>E-Mail</th><td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['EMAIL'];?>
</td></tr>
  <tr><th>携帯電話</th><td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['CELL'];?>
</td></tr>
  <tr><th>TEL</th><td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['TEL'];?>
</td></tr>
  <tr><th>FAX</th><td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['FAX'];?>
</td></tr>
  <tr><th rowspan="2">利用経歴</th>
    <td class="small1">西表島でのダイビングは「<?php echo $_smarty_tpl->tpl_vars['d']->value['IRIHIS'];?>
」</td></tr>
    <tr><td class="small1">当ショップのご利用は「<?php echo $_smarty_tpl->tpl_vars['d']->value['REPEAT'];?>
」</td></tr>
    <tr><th class="B" colspan="2">申し込み内容</th></tr>
<tr>
  <th>参加者</th>
  <td class="small1">
<?php echo $_smarty_tpl->tpl_vars['d']->value['PERSON'];?>
名様
<table class="small1c">
<tr>
  <th class="small1">名前</th>
  <th class="small1">年齢</th>
  <th class="small1">性別</th>
  <th class="small1">ダイビングスタイル</th>
  <th class="small1">ダイビング経験</th>
  <th class="small1">ブランク</th>
</tr>
<?php if ($_smarty_tpl->tpl_vars['d']->value['MEMBER1'] != '') {?>
<tr>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER1'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['AGE1'];?>
 歳</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SEX1'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE1'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL1'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK1'];?>
</td>
</tr>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['MEMBER2'] != '') {?>
<tr>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER2'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['AGE2'];?>
 歳</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SEX2'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE2'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL2'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK2'];?>
</td>
</tr>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['MEMBER3'] != '') {?>
<tr>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER3'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['AGE3'];?>
 歳</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SEX3'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE3'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL3'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK3'];?>
</td>
</tr>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['MEMBER4'] != '') {?>
<tr>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER4'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['AGE4'];?>
 歳</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SEX4'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE4'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL4'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK4'];?>
</td>
</tr>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['MEMBER5'] != '') {?>
<tr>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER5'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['AGE5'];?>
 歳</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SEX5'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE5'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL5'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK5'];?>
</td>
</tr>
<?php }?>

</table>

<tr><th>ダイビング希望日</th><td class="small1">
<?php echo $_smarty_tpl->tpl_vars['d']->value['DIVING_Year'];?>
年<?php echo $_smarty_tpl->tpl_vars['d']->value['DIVING_Month'];?>
月<?php echo $_smarty_tpl->tpl_vars['d']->value['DIVING_Day'];?>
日から
<?php echo $_smarty_tpl->tpl_vars['d']->value['DURING'];?>

<?php if ($_smarty_tpl->tpl_vars['d']->value['DURINGC'] != '') {?>
<br>コメント：<?php echo $_smarty_tpl->tpl_vars['d']->value['DURINGC'];?>

<?php }
if ($_smarty_tpl->tpl_vars['d']->value['TDDIVE'] != "0") {?>
<br>石垣を<?php echo $_smarty_tpl->tpl_vars['d']->value['ISGDEP'];?>
頃出発して<?php echo $_smarty_tpl->tpl_vars['d']->value['TDDIVE'];?>
本くらい到着日に潜りたい。
<?php }?>
</td></tr>
<tr><th>西表島滞在期間</th>
<td class="small1">
<?php echo $_smarty_tpl->tpl_vars['d']->value['STAYIN_Year'];?>
年<?php echo $_smarty_tpl->tpl_vars['d']->value['STAYIN_Month'];?>
月<?php echo $_smarty_tpl->tpl_vars['d']->value['STAYIN_Day'];?>
日に到着して、
<?php echo $_smarty_tpl->tpl_vars['d']->value['BACKOUT_Year'];?>
年<?php echo $_smarty_tpl->tpl_vars['d']->value['BACKOUT_Month'];?>
月<?php echo $_smarty_tpl->tpl_vars['d']->value['BACKOUT_Day'];?>
日に出発する。
</td></tr>

<tr><th class="B" colspan="2">その他情報</th></tr>


<tr>
<th>宿の手配</th>
<td class="small1">
<?php if ($_smarty_tpl->tpl_vars['d']->value['HOTEL'] != "手配済み") {
echo $_smarty_tpl->tpl_vars['d']->value['HOTEL'];?>
<br>
相部屋は、
<?php if ($_smarty_tpl->tpl_vars['d']->value['ROOMSHARE'] == "はい") {?>
OK
<?php } else { ?>
いや
<?php }?>
です。<br>
バス・トイレは、<?php echo $_smarty_tpl->tpl_vars['d']->value['BATHTYPE'];?>

<?php if ($_smarty_tpl->tpl_vars['d']->value['BATHTYPE'] != "おまかせ") {?>
希望
<?php }?>
です。<br>
<?php if ($_smarty_tpl->tpl_vars['d']->value['REQHOTEL1'] != '' || $_smarty_tpl->tpl_vars['d']->value['REQHOTEL2'] != '' || $_smarty_tpl->tpl_vars['d']->value['REQHOTEL3'] != '') {?>
希望のホテルがあります。
<ul>
<?php if ($_smarty_tpl->tpl_vars['d']->value['REQHOTEL1'] != '') {?>
<li><?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL1'];?>
</li>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['REQHOTEL2'] != '') {?>
<li><?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL2'];?>
</li>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['REQHOTEL3'] != '') {?>
<li><?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL3'];?>
</li>
<?php }?>
</ul>
<?php }
} else {
if ($_smarty_tpl->tpl_vars['d']->value['HOTELNAME'] != '') {?>
&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['d']->value['HOTELNAME'];?>
 を手配済み。
<?php }
}
if ($_smarty_tpl->tpl_vars['d']->value['REQHOTELC'] != '') {?>
<br>&nbsp;&nbsp;予算などの希望：<?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTELC'];?>

<?php }?>
</td>
</tr>

<tr><th>その他情報</th>
<td class="small1">
<?php
$__section_rid_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_rid']) ? $_smarty_tpl->tpl_vars['__smarty_section_rid'] : false;
$__section_rid_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['d']->value['REMARKS']) ? count($_loop) : max(0, (int) $_loop));
$__section_rid_0_total = $__section_rid_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_rid'] = new Smarty_Variable(array());
if ($__section_rid_0_total != 0) {
for ($__section_rid_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_rid']->value['index'] = 0; $__section_rid_0_iteration <= $__section_rid_0_total; $__section_rid_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_rid']->value['index']++){
echo $_smarty_tpl->tpl_vars['d']->value['REMARKS'][(isset($_smarty_tpl->tpl_vars['__smarty_section_rid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_rid']->value['index'] : null)];?>
<br>
<?php
}
}
if ($__section_rid_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_rid'] = $__section_rid_0_saved;
}
if ($_smarty_tpl->tpl_vars['d']->value['REMARKSC'] != '') {?>
<br>コメント：<?php echo $_smarty_tpl->tpl_vars['d']->value['REMARKSC'];?>

<?php }?>
</td>
</tr>
<tr>
<th rowspan="2">レンタル機材</th>
<td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['RENTALL'];?>
</td>
</tr>
<tr><td class="small1">コメント：<br>
  <?php echo $_smarty_tpl->tpl_vars['d']->value['RENTALLC'];?>

  </td>
</tr>
<tr><th>連絡事項</th>
<td class="small1">
<?php echo $_smarty_tpl->tpl_vars['d']->value['COMMENT'];?>

</td></tr>
</table>
</div>
</body>
</html>
<?php }
}
