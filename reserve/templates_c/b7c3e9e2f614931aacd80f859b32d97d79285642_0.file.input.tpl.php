<?php
/* Smarty version 3.1.30, created on 2017-03-06 23:29:15
  from "/var/www/vhosts/takedive.com/httpdocs/reserve2/templates/input.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58bd723bd8f956_20661836',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b7c3e9e2f614931aacd80f859b32d97d79285642' => 
    array (
      0 => '/var/www/vhosts/takedive.com/httpdocs/reserve2/templates/input.tpl',
      1 => 1488810469,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58bd723bd8f956_20661836 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_html_options')) require_once '/var/www/vhosts/takedive.com/libs/plugins/function.html_options.php';
if (!is_callable('smarty_function_html_select_date')) require_once '/var/www/vhosts/takedive.com/libs/plugins/function.html_select_date.php';
if (!is_callable('smarty_function_html_checkboxes')) require_once '/var/www/vhosts/takedive.com/libs/plugins/function.html_checkboxes.php';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
 <link rel="stylesheet" type="text/css" href="default.css">
 <?php echo '<script'; ?>
 type="text/javascript" src="xmlhttp.js"><?php echo '</script'; ?>
>
 <?php echo '<script'; ?>
 type="text/javascript" src="res.js"><?php echo '</script'; ?>
>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>TAKE DIVING/予約申込</title>
 <link rel="shortcut icon" href="../favicon.ico">
</head>
<body bgcolor="#66ffff">
<div align="center">
<IMG src="reserve.gif" width="268" height="135" alt="お申し込みフォーム" border="0"></div>
<br>
<table class="caution">
<tr><td>
<center>ご注意</center>
<ul>
<li>英数字は半角で入力してください。 <br>＜例＞ＩＲＩＯＭＯＴＥ=×　IRIOMOTE iriomote=○</li>
<li>カタカナは全角で記入してください。半角カナは使用できません。 </li>
<li>JavaScriptをONにしてご利用ください。</li>
<li>メールアドレスに間違いがありますと、こちらから確認のメールを差し上げることが出来ません。
即時返信される確認メールを受信できない場合はメールアドレス入力に間違いがある可能性が高いので、
再度確認していただき送信していただくか、ご連絡ください。</li>
<li>弊社にて受信確認後、48時間以内にメール、またはお電話にて確認させていただきます。
確認が取れました時点で予約成立とさせていただきます。</li>
<li>キャンセルまたは変更される場合には、出来るだけ早くご連絡をお願いします。
キャンセル料はご予約日の一週間前から発生します。<br><a href="cancel-policy.html" target="CANCELPOLICY">※キャンセルポリシー</a></li>
<li>カヌーなど、陸観光を予定されている方は、船の定員がありますので、
できるだけ先にダイビングの日程を決めていただけると助かります。<br>
<a href="http://www.takedive.com/cgi-bin/yoyaku.cgi?mode=show" target="CALENDAR">予約状況カレンダーはこちら。</a></li>
</ul>
</td></tr>
</table>

<HR width="100%" align="center">
<!-- <p>西表島ニラカナイへご宿泊で、ダイビングを申込んでいただこうとしているお客さまへ。</p>
<p>まず、こちらをごらんください。</p> -->

<form method="POST" name="INPUT" action="confirm.php">
<div align="center">
<table border="0">
<tr><th class="B" colspan="3">申込者(代表者)連絡先</th></tr>
<tr><th rowspan="2">氏名</th><th>漢字</th>
<td><input type="TEXT" name="NAME" size="44" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['NAME'];?>
" onChange="copy2member(this,document.INPUT.MEMBER1)">
<?php if ($_smarty_tpl->tpl_vars['e']->value['NAME'] != '') {?>
<span style="color: #ff0000;"><?php echo $_smarty_tpl->tpl_vars['e']->value['NAME'];?>
</span>
<?php }?>
</td></tr>
<tr><th><font size="-2">ふりがな</font></th><td><input type="TEXT" name="KANA" size="44" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['KANA'];?>
">
<?php if ($_smarty_tpl->tpl_vars['e']->value['KANA'] != '') {?>
<span style="color: #ff0000;"><?php echo $_smarty_tpl->tpl_vars['e']->value['KANA'];?>
</span>
<?php }?>
</td></tr>
<tr><th colspan="2">性別年齢</th>
    <td class="small1">性別
<?php echo smarty_function_html_options(array('name'=>"SEX",'options'=>$_smarty_tpl->tpl_vars['sexOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['SEX']),$_smarty_tpl);?>

&nbsp;&nbsp;
年齢<input type="TEXT" name="AGE" size="2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE'];?>
" onChange="copy2member(this,document.INPUT.AGE1)">歳</td></tr>
<tr><th rowspan="2">住所</th><th>〒</th><td class="small1"> <input type="TEXT" name="ZIP" size="7" maxsize="7" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['ZIP'];?>
" onKeyUp="zipMatch(this)"> 半角数字だけで入力してください。</td></tr>
<tr><th><br></th><td><div class="small1" id="kouho"></div><input type="TEXT" name="ADDR" size="44" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['ADDR'];?>
"></td></tr>

<tr>
<th colspan="2">E-Mail</th><td><input type="TEXT" name="EMAIL" size="44" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['EMAIL'];?>
" onChange="mailAcheck(this)">
<?php if ($_smarty_tpl->tpl_vars['e']->value['EMAIL'] != '') {?>
<span style="color: #ff0000;"><?php echo $_smarty_tpl->tpl_vars['e']->value['EMAIL'];?>
</span>
<?php }?>
</td>
</tr>
<tr>
<th colspan="2">携帯電話</th>
<td class="small1">
<input type="TEXT" name="CELL" size="13" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['CELL'];?>
" onChange="cellNcheck(this)">
<?php if ($_smarty_tpl->tpl_vars['e']->value['CELL'] != '') {?>
<span style="color: #ff0000;"><?php echo $_smarty_tpl->tpl_vars['e']->value['CELL'];?>
</span>
<?php }?>
</td></tr>
<th colspan="2">TEL</th>
<td class="small1">
<input type="TEXT" name="TEL" size="13" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['TEL'];?>
" onChange="telNcheck(this)">
<?php if ($_smarty_tpl->tpl_vars['e']->value['TEL'] != '') {?>
<span style="color: #ff0000;"><?php echo $_smarty_tpl->tpl_vars['e']->value['TEL'];?>
</span>
<?php }?>
</td></tr>
<tr><th colspan="2">FAX</th><td><input type="TEXT" name="FAX" size="13" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['FAX'];?>
" onChange="telNcheck(this)"></td></tr>
<tr><th rowspan="2" colspan="2">利用経歴</th>
<td class="small1">西表島でのダイビングは
<?php echo smarty_function_html_options(array('name'=>'IRIHIS','options'=>$_smarty_tpl->tpl_vars['repeatOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['IRIHIS']),$_smarty_tpl);?>

</td></tr>
<tr><td class="small1">当ショップのご利用は
<?php echo smarty_function_html_options(array('name'=>'REPEAT','options'=>$_smarty_tpl->tpl_vars['repeatOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['REPEAT']),$_smarty_tpl);?>

</td></tr>
<tr><th class="B" colspan="3">申し込み内容</th></tr>
<tr>
  <th rowspan="2" colspan="2">参加者</th>
  <td class="small1">
<p>ご自身を含めて、一緒にダイビングに来る人を書いてください。<br>
連絡の都合もありますので名前と年齢は書いてくださいね。<br>
5名以上の場合にはコメント欄に人数を記入ください。<br>
組み合わせなどリクエストはコメント欄に記入ください。
</p></td></tr>
<tr><td class="small1">
<table class="small1">
<tr>
  <th class="small1">名前</th>
  <th class="small1">年齢</th>
  <th class="small1">性別</th>
  <th class="small1">ダイビングスタイル</th>
  <th class="small1">ダイビング経験</th>
  <th class="small1">ブランク</th>
</tr>
<tr>
<td><input type="TEXT" name="MEMBER1" size="15" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER1'];?>
"></td>
<td><input type="TEXT" name="AGE1" size="2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE1'];?>
">歳</td>
<td><?php echo smarty_function_html_options(array('name'=>"SEX1",'options'=>$_smarty_tpl->tpl_vars['sexOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['SEX1']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"STYLE1",'options'=>$_smarty_tpl->tpl_vars['styleOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['STYLE1']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"SKILL1",'options'=>$_smarty_tpl->tpl_vars['skilOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['SKILL1']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"BLANK1",'options'=>$_smarty_tpl->tpl_vars['blankOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['BLANK1']),$_smarty_tpl);?>
</td>
</tr>

<tr>
<td><input type="TEXT" name="MEMBER2" size="15" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER2'];?>
"></td>
<td><input type="TEXT" name="AGE2" size="2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE2'];?>
">歳</td>
<td><?php echo smarty_function_html_options(array('name'=>"SEX2",'options'=>$_smarty_tpl->tpl_vars['sexOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['SEX2']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"STYLE2",'options'=>$_smarty_tpl->tpl_vars['styleOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['STYLE2']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"SKILL2",'options'=>$_smarty_tpl->tpl_vars['skilOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['SKILL2']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"BLANK2",'options'=>$_smarty_tpl->tpl_vars['blankOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['BLANK2']),$_smarty_tpl);?>
</td>
</tr>

<tr>
<td><input type="TEXT" name="MEMBER3" size="15" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER3'];?>
"></td>
<td><input type="TEXT" name="AGE3" size="2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE3'];?>
">歳</td>
<td><?php echo smarty_function_html_options(array('name'=>"SEX3",'options'=>$_smarty_tpl->tpl_vars['sexOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['SEX3']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"STYLE3",'options'=>$_smarty_tpl->tpl_vars['styleOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['STYLE3']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"SKILL3",'options'=>$_smarty_tpl->tpl_vars['skilOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['SKILL3']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"BLANK3",'options'=>$_smarty_tpl->tpl_vars['blankOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['BLANK3']),$_smarty_tpl);?>
</td>
</tr>

<tr>
<td><input type="TEXT" name="MEMBER4" size="15" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER4'];?>
"></td>
<td><input type="TEXT" name="AGE4" size="2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE4'];?>
">歳</td>
<td><?php echo smarty_function_html_options(array('name'=>"SEX4",'options'=>$_smarty_tpl->tpl_vars['sexOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['SEX4']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"STYLE4",'options'=>$_smarty_tpl->tpl_vars['styleOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['STYLE4']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"SKILL4",'options'=>$_smarty_tpl->tpl_vars['skilOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['SKILL4']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"BLANK4",'options'=>$_smarty_tpl->tpl_vars['blankOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['BLANK4']),$_smarty_tpl);?>
</td>
</tr>

<tr>
<td><input type="TEXT" name="MEMBER5" size="15" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER5'];?>
"></td>
<td><input type="TEXT" name="AGE5" size="2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE5'];?>
">歳</td>
<td><?php echo smarty_function_html_options(array('name'=>"SEX5",'options'=>$_smarty_tpl->tpl_vars['sexOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['SEX5']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"STYLE5",'options'=>$_smarty_tpl->tpl_vars['styleOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['STYLE5']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"SKILL5",'options'=>$_smarty_tpl->tpl_vars['skilOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['SKILL5']),$_smarty_tpl);?>
</td>
<td><?php echo smarty_function_html_options(array('name'=>"BLANK5",'options'=>$_smarty_tpl->tpl_vars['blankOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['BLANK5']),$_smarty_tpl);?>
</td>
</tr>

</table>
<p class="caution">ファンダイブ以外は常時開催しているわけではございませんので、<br>
日程などをお尋ねください。</p>
</td>
</tr>
<tr><th colspan="2">ダイビング希望日</th>
<td class="small1">
<p>ご存じの通り、ダイビング終了後12時間は飛行機に乗ることができませんので、<br>
帰りのスケジュールは注意してください。</p>
<?php echo smarty_function_html_select_date(array('prefix'=>"DIVING_",'field_order'=>"YMD",'end_year'=>"+1",'month_format'=>"%m月",'day_format'=>"%d日"),$_smarty_tpl);?>

から
<?php echo smarty_function_html_options(array('name'=>'DURING','options'=>$_smarty_tpl->tpl_vars['duringOpt']->value),$_smarty_tpl);?>

<p>到着日にダイビングをご希望の方は、石垣港発の船の時間と希望するダイビング本数をお知らせください。<br>
<input type="TEXT" name="ISGDEP" size="5" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['ISGDEP'];?>
">発
&nbsp;&nbsp;
<?php echo smarty_function_html_options(array('name'=>"TDDIVE",'options'=>$_smarty_tpl->tpl_vars['num123Opt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['TDDIVE']),$_smarty_tpl);?>
本くらいを希望</p>
<p>滞在中にダイビング以外の予定が有る場合はコメント欄にご記入ください。
  (日程が決まっている場合はなるべく具体的にお願いします。)<br>
コメント欄：<input type="TEXT" name="DURINGC"  size="40"value="<?php echo $_smarty_tpl->tpl_vars['d']->value['DURINGC'];?>
"></p>
</td></tr>
<tr><th colspan="2">西表島滞在期間</th>
<td class="small1">
<?php echo smarty_function_html_select_date(array('prefix'=>"STAYIN_",'field_order'=>"YMD",'end_year'=>"+1",'month_format'=>"%m月",'day_format'=>"%d日"),$_smarty_tpl);?>

に到着して、
<?php echo smarty_function_html_select_date(array('prefix'=>"BACKOUT_",'field_order'=>"YMD",'end_year'=>"+1",'month_format'=>"%m月",'day_format'=>"%d日"),$_smarty_tpl);?>

に出発</td></tr>

<tr><th class="B" colspan="3">その他情報</th></tr>

<tr><th colspan="2">宿の手配</th>
<td class="small1">
<p>宿泊先は、
<?php echo smarty_function_html_options(array('name'=>"HOTEL",'options'=>$_smarty_tpl->tpl_vars['hotelOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['HOTEL']),$_smarty_tpl);?>
</p>
<p>手配済みの場合、宿の名称を記入してください。<br>
送迎を行っていない地域もございます。<br>
&nbsp;&nbsp;&nbsp;&nbsp;<input type="TEXT" name="HOTELNAME" size="44" value="<?php echo $_smarty_tpl->tpl_vars['HOTELNAME']->value;?>
"></p>
<p class="small1">依頼を希望される場合には、詳細な希望をご指定ください。<br>
ご希望にかなわない場合もございますのでご了承ください。</p>
<table border="0">
<tr><td>相部屋でよいですか？</td>
<td><?php echo smarty_function_html_options(array('name'=>"ROOMSHARE",'options'=>$_smarty_tpl->tpl_vars['yesnoOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['ROOMSHARE']),$_smarty_tpl);?>
</td></tr>
<tr><td>バス・トイレはどうされますか？</td>
<td><?php echo smarty_function_html_options(array('name'=>"BATHTYPE",'options'=>$_smarty_tpl->tpl_vars['bathOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['BATHTYPE']),$_smarty_tpl);?>
</td></tr>
<tr><td rowspan="3">希望の宿がありますか？<br><a href="hotel-info.html" target="HOTELINFO">宿の情報ページ</a></td>
<td>第1希望:<input type="TEXT" name="REQHOTEL1" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL1'];?>
"></td></tr>
<tr><td>第2希望:<input type="TEXT" name="REQHOTEL2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL2'];?>
"></td></tr>
<tr><td>第3希望:<input type="TEXT" name="REQHOTEL3" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL3'];?>
"></td></tr>
<tr><td>※予算などございましたらご記入ください。</td>
<td><input type="TEXT" name="REQHOTELC" size="20" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTELC'];?>
"></td></tr>
</table>
</td></tr>

<tr><th colspan="2">その他情報</th>
<td class="small1">
<p>ガイドのスケジュールの参考になりますので、<br>
以下の項目もチェックしてください。<br>
なお、最後の「見たい生物」などは具体的な部分をコメントにご記入願います。</p>
<?php echo smarty_function_html_checkboxes(array('name'=>"REMARKS",'separator'=>'<br>','values'=>$_smarty_tpl->tpl_vars['remarks']->value,'output'=>$_smarty_tpl->tpl_vars['remarks']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['REMARKS']),$_smarty_tpl);?>

<p>コメント欄：<input type="TEXT" name="REMARKSC" size="40" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REMARKSC'];?>
"></P>
</td>
</tr>
<tr><th colspan="2">レンタル機材</th>
<td class="small1">
<p>ウェイトなどは通常準備しております。<br>
サイズの問題もありますので、必要な機材がありましたら、<br>
連絡事項に記入願います。</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo smarty_function_html_options(array('name'=>'RENTALL','options'=>$_smarty_tpl->tpl_vars['rentallOpt']->value,'selected'=>$_smarty_tpl->tpl_vars['d']->value['RENTALL']),$_smarty_tpl);?>
</p>
<p>コメント欄：<input type="TEXT" name="RENTALLC" size="40" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['RENTALLC'];?>
"></p>
</td></tr>
<tr><th colspan="2">連絡事項</th>
<td><textarea name="COMMENT" cols="40">
<?php echo $_smarty_tpl->tpl_vars['COMMENT']->value;?>

</textarea>
</td></tr>
</table>
<input type="SUBMIT" name="GO" value="予約する">
<!-- <input type="SUBMIT" name="DEB" value="デバッグ出力"> -->
</div>
</form>
</body>
</html>
<?php }
}
