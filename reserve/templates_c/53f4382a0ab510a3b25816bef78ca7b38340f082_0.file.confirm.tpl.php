<?php
/* Smarty version 3.1.30, created on 2017-03-07 00:07:02
  from "/var/www/vhosts/takedive.com/httpdocs/reserve/templates/confirm.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58bd7b16656af2_33724980',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '53f4382a0ab510a3b25816bef78ca7b38340f082' => 
    array (
      0 => '/var/www/vhosts/takedive.com/httpdocs/reserve/templates/confirm.tpl',
      1 => 1488810468,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58bd7b16656af2_33724980 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
 <link rel="stylesheet" type="text/css" href="default.css">
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>TAKE DIVING/予約確認</title>
 <link rel="shortcut icon" href="../favicon.ico">
</head>
<body bgcolor="#66ffff">
<div align="center">
<IMG src="reserve.gif" width="268" height="135" alt="確認フォーム" border="0"></div>
<P align="center">以下のように予約を承ります。ご確認ください。<br>
OKならば、一番したの「本当に予約する」ボタンを押してください。</p>

<HR width="100%" align="center">
<!-- <p>西表島ニラカナイへご宿泊で、ダイビングを申込んでいただこうとしているお客さまへ。</p>
<p>まず、こちらをごらんください。</p> -->

<form method="POST" action="index.php" name="CONFIRM">
<input type="HIDDEN" name="NAME" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['NAME'];?>
">
<input type="HIDDEN" name="KANA" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['KANA'];?>
">
<input type="HIDDEN" name="SEX" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['SEX'];?>
">
<input type="HIDDEN" name="AGE" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE'];?>
">
<input type="HIDDEN" name="ZIP" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['ZIP'];?>
">
<input type="HIDDEN" name="ADDR" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['ADDR'];?>
">
<input type="HIDDEN" name="EMAIL" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['EMAIL'];?>
">
<input type="HIDDEN" name="CELL" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['CELL'];?>
">
<input type="HIDDEN" name="TEL" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['TEL'];?>
">
<input type="HIDDEN" name="FAX" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['FAX'];?>
">
<input type="HIDDEN" name="IRIHIS" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['IRIHIS'];?>
">
<input type="HIDDEN" name="REPEAT" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REPEAT'];?>
">
<input type="HIDDEN" name="RENTALL" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['RENTALL'];?>
">
<input type="HIDDEN" name="RENTALLC" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['RENTALLC'];?>
">
<input type="HIDDEN" name="HOTEL" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['HOTEL'];?>
">
<input type="HIDDEN" name="HOTELNAME" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['HOTELNAME'];?>
">
<input type="HIDDEN" name="ROOMSHARE" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['ROOMSHARE'];?>
">
<input type="HIDDEN" name="BATHTYPE" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['BATHTYPE'];?>
">
<input type="HIDDEN" name="REQHOTEL1" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL1'];?>
">
<input type="HIDDEN" name="REQHOTEL2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL2'];?>
">
<input type="HIDDEN" name="REQHOTEL3" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL3'];?>
">
<input type="HIDDEN" name="REQHOTELC" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTELC'];?>
">
<input type="HIDDEN" name="DURING" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['DURING'];?>
">
<input type="HIDDEN" name="DURINGC" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['DURINGC'];?>
">
<input type="HIDDEN" name="TDDIVE" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['TDDIVE'];?>
">
<input type="HIDDEN" name="ISGDEP" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['ISGDEP'];?>
">
<input type="HIDDEN" name="STAYIN_Year" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['STAYIN_Year'];?>
">
<input type="HIDDEN" name="STAYIN_Month" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['STAYIN_Month'];?>
">
<input type="HIDDEN" name="STAYIN_Day" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['STAYIN_Day'];?>
">
<input type="HIDDEN" name="DIVING_Year" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['DIVING_Year'];?>
">
<input type="HIDDEN" name="DIVING_Month" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['DIVING_Month'];?>
">
<input type="HIDDEN" name="DIVING_Day" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['DIVING_Day'];?>
">
<input type="HIDDEN" name="BACKOUT_Year" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['BACKOUT_Year'];?>
">
<input type="HIDDEN" name="BACKOUT_Month" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['BACKOUT_Month'];?>
">
<input type="HIDDEN" name="BACKOUT_Day" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['BACKOUT_Day'];?>
">
<input type="HIDDEN" name="REMARKSC" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REMARKSC'];?>
">
<input type="HIDDEN" name="COMMENT" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['COMMENT'];?>
">
<input type="HIDDEN" name="PERSON" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['PERSON'];?>
">

<div align="center">
<table border="0">
<tr><th class="B" colspan="2">申込者(代表者)連絡先</th></tr>
<tr><th>氏名</th><td><span class="rubi"><?php echo $_smarty_tpl->tpl_vars['d']->value['KANA'];?>
</span><br><?php echo $_smarty_tpl->tpl_vars['d']->value['NAME'];?>
</td></tr>
<tr><th>性別年齢</th><td class="small1">性別：<?php echo $_smarty_tpl->tpl_vars['d']->value['SEX'];?>
&nbsp;&nbsp;年齢：<?php echo (($tmp = @$_smarty_tpl->tpl_vars['d']->value['AGE'])===null||$tmp==='' ? '？' : $tmp);?>
歳</td></tr>
<tr><th>住所</th><td class="small1">〒<?php echo $_smarty_tpl->tpl_vars['d']->value['ZIP'];?>
 <?php echo $_smarty_tpl->tpl_vars['d']->value['ADDR'];?>
</td></tr>
<tr><th>E-Mail</th><td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['EMAIL'];?>
</td></tr>
<tr><th>携帯電話</th><td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['CELL'];?>
</td></tr>
<tr><th>TEL</th><td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['TEL'];?>
</td></tr>
<tr><th>FAX</th><td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['FAX'];?>
</td></tr>
<tr><th rowspan="2">利用経歴</th>
<td class="small1">西表島でのダイビングは「<?php echo $_smarty_tpl->tpl_vars['d']->value['IRIHIS'];?>
」</td></tr>
<tr><td class="small1">当ショップのご利用は「<?php echo $_smarty_tpl->tpl_vars['d']->value['REPEAT'];?>
」</td></tr>
<tr><th class="B" colspan="2">申し込み内容</th></tr>
<tr>
  <th>参加者</th>
  <td class="small1">
<?php echo $_smarty_tpl->tpl_vars['d']->value['PERSON'];?>
名様
<table class="small1c">
<tr>
  <th class="small1">名前</th>
  <th class="small1">年齢</th>
  <th class="small1">性別</th>
  <th class="small1">ダイビングスタイル</th>
  <th class="small1">ダイビング経験</th>
  <th class="small1">ブランク</th>
</tr>
<?php if ($_smarty_tpl->tpl_vars['d']->value['MEMBER1'] != '') {?>
<input type="HIDDEN" name="MEMBER1" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER1'];?>
">
<input type="HIDDEN" name="AGE1" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE1'];?>
">
<input type="HIDDEN" name="SEX1" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['SEX1'];?>
">
<input type="HIDDEN" name="STYLE1" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE1'];?>
">
<input type="HIDDEN" name="SKILL1" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL1'];?>
">
<input type="HIDDEN" name="BLANK1" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK1'];?>
">
<tr>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER1'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['AGE1'];?>
 歳</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SEX1'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE1'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL1'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK1'];?>
</td>
</tr>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['MEMBER2'] != '') {?>
<input type="HIDDEN" name="MEMBER2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER2'];?>
">
<input type="HIDDEN" name="AGE2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE2'];?>
">
<input type="HIDDEN" name="SEX2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['SEX2'];?>
">
<input type="HIDDEN" name="STYLE2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE2'];?>
">
<input type="HIDDEN" name="SKILL2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL2'];?>
">
<input type="HIDDEN" name="BLANK2" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK2'];?>
">
<tr>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER2'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['AGE2'];?>
 歳</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SEX2'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE2'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL2'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK2'];?>
</td>
</tr>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['MEMBER3'] != '') {?>
<input type="HIDDEN" name="MEMBER3" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER3'];?>
">
<input type="HIDDEN" name="AGE3" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE3'];?>
">
<input type="HIDDEN" name="SEX3" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['SEX3'];?>
">
<input type="HIDDEN" name="STYLE3" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE3'];?>
">
<input type="HIDDEN" name="SKILL3" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL3'];?>
">
<input type="HIDDEN" name="BLANK3" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK3'];?>
">
<tr>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER3'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['AGE3'];?>
 歳</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SEX3'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE3'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL3'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK3'];?>
</td>
</tr>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['MEMBER4'] != '') {?>
<input type="HIDDEN" name="MEMBER4" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER4'];?>
">
<input type="HIDDEN" name="AGE4" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE4'];?>
">
<input type="HIDDEN" name="SEX4" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['SEX4'];?>
">
<input type="HIDDEN" name="STYLE4" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE4'];?>
">
<input type="HIDDEN" name="SKILL4" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL4'];?>
">
<input type="HIDDEN" name="BLANK4" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK4'];?>
">
<tr>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER4'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['AGE4'];?>
 歳</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SEX4'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE4'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL4'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK4'];?>
</td>
</tr>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['MEMBER5'] != '') {?>
<input type="HIDDEN" name="MEMBER5" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER5'];?>
">
<input type="HIDDEN" name="AGE5" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['AGE5'];?>
">
<input type="HIDDEN" name="SEX5" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['SEX5'];?>
">
<input type="HIDDEN" name="STYLE5" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE5'];?>
">
<input type="HIDDEN" name="SKILL5" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL5'];?>
">
<input type="HIDDEN" name="BLANK5" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK5'];?>
">
<tr>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['MEMBER5'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['AGE5'];?>
 歳</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SEX5'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['STYLE5'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['SKILL5'];?>
</td>
<td><?php echo $_smarty_tpl->tpl_vars['d']->value['BLANK5'];?>
</td>
</tr>
<?php }?>

</table>

<tr><th>ダイビング希望日</th><td class="small1">
<?php echo $_smarty_tpl->tpl_vars['d']->value['DIVING_Year'];?>
年<?php echo $_smarty_tpl->tpl_vars['d']->value['DIVING_Month'];?>
月<?php echo $_smarty_tpl->tpl_vars['d']->value['DIVING_Day'];?>
日から
<?php echo $_smarty_tpl->tpl_vars['d']->value['DURING'];?>

<?php if ($_smarty_tpl->tpl_vars['d']->value['DURINGC'] != '') {?>
<br>コメント：<?php echo $_smarty_tpl->tpl_vars['d']->value['DURINGC'];?>

<?php }
if ($_smarty_tpl->tpl_vars['d']->value['TDDIVE'] != "0") {?>
<br>石垣を<?php echo $_smarty_tpl->tpl_vars['d']->value['ISGDEP'];?>
頃出発して<?php echo $_smarty_tpl->tpl_vars['d']->value['TDDIVE'];?>
本くらい到着日に潜りたい。
<?php }?>
</td></tr>
<tr><th>西表島滞在期間</th>
<td class="small1">
<?php echo $_smarty_tpl->tpl_vars['d']->value['STAYIN_Year'];?>
年<?php echo $_smarty_tpl->tpl_vars['d']->value['STAYIN_Month'];?>
月<?php echo $_smarty_tpl->tpl_vars['d']->value['STAYIN_Day'];?>
日に到着して、
<?php echo $_smarty_tpl->tpl_vars['d']->value['BACKOUT_Year'];?>
年<?php echo $_smarty_tpl->tpl_vars['d']->value['BACKOUT_Month'];?>
月<?php echo $_smarty_tpl->tpl_vars['d']->value['BACKOUT_Day'];?>
日に出発する。
</td></tr>

<tr><th class="B" colspan="2">その他情報</th></tr>


<tr>
<th>宿の手配</th>
<td class="small1">
<?php if ($_smarty_tpl->tpl_vars['d']->value['HOTEL'] != "手配済み") {
echo $_smarty_tpl->tpl_vars['d']->value['HOTEL'];?>
<br>
相部屋は、
<?php if ($_smarty_tpl->tpl_vars['d']->value['ROOMSHARE'] == "はい") {?>
OK
<?php } else { ?>
いや
<?php }?>
です。<br>
バス・トイレは、<?php echo $_smarty_tpl->tpl_vars['d']->value['BATHTYPE'];?>

<?php if ($_smarty_tpl->tpl_vars['d']->value['BATHTYPE'] != "おまかせ") {?>
希望
<?php }?>
です。<br>
<?php if ($_smarty_tpl->tpl_vars['d']->value['REQHOTEL1'] != '' || $_smarty_tpl->tpl_vars['d']->value['REQHOTEL2'] != '' || $_smarty_tpl->tpl_vars['d']->value['REQHOTEL3'] != '') {?>
希望のホテルがあります。
<ul>
<?php if ($_smarty_tpl->tpl_vars['d']->value['REQHOTEL1'] != '') {?>
<li><?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL1'];?>
</li>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['REQHOTEL2'] != '') {?>
<li><?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL2'];?>
</li>
<?php }
if ($_smarty_tpl->tpl_vars['d']->value['REQHOTEL3'] != '') {?>
<li><?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTEL3'];?>
</li>
<?php }?>
</ul>
<?php }
} else {
echo $_smarty_tpl->tpl_vars['d']->value['HOTEL'];?>

<?php if ($_smarty_tpl->tpl_vars['d']->value['HOTELNAME'] != '') {?>
&nbsp;&nbsp;<?php echo $_smarty_tpl->tpl_vars['d']->value['HOTELNAME'];?>

<?php }
}
if ($_smarty_tpl->tpl_vars['d']->value['REQHOTELC'] != '') {?>
<br>&nbsp;&nbsp;予算などの希望：<?php echo $_smarty_tpl->tpl_vars['d']->value['REQHOTELC'];?>

<?php }?>
</td>
</tr>

<tr><th>その他情報</th>
<td class="small1">
<?php
$__section_rid_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_rid']) ? $_smarty_tpl->tpl_vars['__smarty_section_rid'] : false;
$__section_rid_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['d']->value['REMARKS']) ? count($_loop) : max(0, (int) $_loop));
$__section_rid_0_total = $__section_rid_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_rid'] = new Smarty_Variable(array());
if ($__section_rid_0_total != 0) {
for ($__section_rid_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_rid']->value['index'] = 0; $__section_rid_0_iteration <= $__section_rid_0_total; $__section_rid_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_rid']->value['index']++){
echo $_smarty_tpl->tpl_vars['d']->value['REMARKS'][(isset($_smarty_tpl->tpl_vars['__smarty_section_rid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_rid']->value['index'] : null)];?>
<br>
<input type="HIDDEN" name="REMARKS[]" value="<?php echo $_smarty_tpl->tpl_vars['d']->value['REMARKS'][(isset($_smarty_tpl->tpl_vars['__smarty_section_rid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_rid']->value['index'] : null)];?>
">
<?php
}
}
if ($__section_rid_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_rid'] = $__section_rid_0_saved;
}
if ($_smarty_tpl->tpl_vars['d']->value['REMARKSC'] != '') {?>
<br>コメント：<?php echo $_smarty_tpl->tpl_vars['d']->value['REMARKSC'];?>

p<?php }?>
</td>
</tr>
<tr>
<th rowspan="2">レンタル機材</th>
<td class="small1"><?php echo $_smarty_tpl->tpl_vars['d']->value['RENTALL'];?>
</td>
</tr>
<tr><td class="small1">コメント：<br>
  <?php echo $_smarty_tpl->tpl_vars['d']->value['RENTALLC'];?>

  </td>
</tr>
<tr><th>連絡事項</th>
<td class="small1">
<?php echo $_smarty_tpl->tpl_vars['d']->value['COMMENT'];?>

</td></tr>
</table>
<input type="SUBMIT" name="GO" value="本当に予約する">
</div>
</form>
</body>
</html>
<?php }
}
