<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
 <link rel="stylesheet" type="text/css" href="default.css">
 <script type="text/javascript" src="xmlhttp.js"></script>
 <script type="text/javascript" src="res.js"></script>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>TAKE DIVING/予約申込</title>
 <link rel="shortcut icon" href="../favicon.ico">
</head>
<body bgcolor="#66ffff">
<div align="center">
<IMG src="reserve.gif" width="268" height="135" alt="お申し込みフォーム" border="0"></div>
<br>
<table class="caution">
<tr><td>
<center>ご注意</center>
<ul>
<li>英数字は半角で入力してください。 <br>＜例＞ＩＲＩＯＭＯＴＥ=×　IRIOMOTE iriomote=○</li>
<li>カタカナは全角で記入してください。半角カナは使用できません。 </li>
<li>JavaScriptをONにしてご利用ください。</li>
<li>メールアドレスに間違いがありますと、こちらから確認のメールを差し上げることが出来ません。
即時返信される確認メールを受信できない場合はメールアドレス入力に間違いがある可能性が高いので、
再度確認していただき送信していただくか、ご連絡ください。</li>
<li>弊社にて受信確認後、48時間以内にメール、またはお電話にて確認させていただきます。
確認が取れました時点で予約成立とさせていただきます。</li>
<li>キャンセルまたは変更される場合には、出来るだけ早くご連絡をお願いします。
キャンセル料はご予約日の一週間前から発生します。<br><a href="cancel-policy.html" target="CANCELPOLICY">※キャンセルポリシー</a></li>
<li>カヌーなど、陸観光を予定されている方は、船の定員がありますので、
できるだけ先にダイビングの日程を決めていただけると助かります。<br>
<a href="http://www.takedive.com/cgi-bin/yoyaku.cgi?mode=show" target="CALENDAR">予約状況カレンダーはこちら。</a>
</li>
</ul>
</td></tr>
</table>

<HR width="100%" align="center">
<!-- <p>西表島ニラカナイへご宿泊で、ダイビングを申込んでいただこうとしているお客さまへ。</p>
<p>まず、こちらをごらんください。</p> -->

<form method="POST" name="INPUT" action="confirm.php">
<div align="center">
<table border="0">
<tr><th class="B" colspan="3">申込者(代表者)連絡先</th></tr>
<tr><th rowspan="2">氏名</th><th>漢字</th>
<td><input type="TEXT" name="NAME" size="44" value="{$d.NAME}" onChange="copy2member(this,document.INPUT.MEMBER1)">
{if $e.NAME != ""}
<span style="color: #ff0000;">{$e.NAME}</span>
{/if}
</td></tr>
<tr><th><font size="-2">ふりがな</font></th><td><input type="TEXT" name="KANA" size="44" value="{$d.KANA}">
{if $e.KANA != ""}
<span style="color: #ff0000;">{$e.KANA}</span>
{/if}
</td></tr>
<tr><th colspan="2">性別年齢</th>
    <td class="small1">性別
{html_options name="SEX" options=$sexOpt selected=$d.SEX}
&nbsp;&nbsp;
年齢<input type="TEXT" name="AGE" size="2" value="{$d.AGE}" onChange="copy2member(this,document.INPUT.AGE1)">歳</td></tr>
<tr><th rowspan="2">住所</th><th>〒</th><td class="small1"> <input type="TEXT" name="ZIP" size="7" maxsize="7" value="{$d.ZIP}" onKeyUp="zipMatch(this)"> 半角数字だけで入力してください。</td></tr>
<tr><th><br></th><td><div class="small1" id="kouho"></div><input type="TEXT" name="ADDR" size="44" value="{$d.ADDR}"></td></tr>

<tr>
<th colspan="2">E-Mail</th><td><input type="TEXT" name="EMAIL" size="44" value="{$d.EMAIL}" onChange="mailAcheck(this)">
{if $e.EMAIL != ""}
<span style="color: #ff0000;">{$e.EMAIL}</span>
{/if}
</td>
</tr>
<tr>
<th colspan="2">携帯電話</th>
<td class="small1">
<input type="TEXT" name="CELL" size="13" value="{$d.CELL}" onChange="cellNcheck(this)">
{if $e.CELL != ""}
<span style="color: #ff0000;">{$e.CELL}</span>
{/if}
</td></tr>
<th colspan="2">TEL</th>
<td class="small1">
<input type="TEXT" name="TEL" size="13" value="{$d.TEL}" onChange="telNcheck(this)">
{if $e.TEL != ""}
<span style="color: #ff0000;">{$e.TEL}</span>
{/if}
</td></tr>
<tr><th colspan="2">FAX</th><td><input type="TEXT" name="FAX" size="13" value="{$d.FAX}" onChange="telNcheck(this)"></td></tr>
<tr><th rowspan="2" colspan="2">利用経歴</th>
<td class="small1">西表島でのダイビングは
{html_options name=IRIHIS options=$repeatOpt selected=$d.IRIHIS}
</td></tr>
<tr><td class="small1">当ショップのご利用は
{html_options name=REPEAT options=$repeatOpt selected=$d.REPEAT }
</td></tr>
<tr><th class="B" colspan="3">申し込み内容</th></tr>
<tr>
  <th rowspan="2" colspan="2">参加者</th>
  <td class="small1">
<p>ご自身を含めて、一緒にダイビングに来る人を書いてください。<br>
連絡の都合もありますので名前と年齢は書いてくださいね。<br>
5名以上の場合にはコメント欄に人数を記入ください。<br>
組み合わせなどリクエストはコメント欄に記入ください。
</p></td></tr>
<tr><td class="small1">
<table class="small1">
<tr>
  <th class="small1">名前</th>
  <th class="small1">年齢</th>
  <th class="small1">性別</th>
  <th class="small1">ダイビングスタイル</th>
  <th class="small1">ダイビング経験</th>
  <th class="small1">ブランク</th>
</tr>
<tr>
<td><input type="TEXT" name="MEMBER1" size="15" value="{$d.MEMBER1}"></td>
<td><input type="TEXT" name="AGE1" size="2" value="{$d.AGE1}">歳</td>
<td>{html_options name="SEX1" options=$sexOpt selected=$d.SEX1}</td>
<td>{html_options name="STYLE1" options=$styleOpt selected=$d.STYLE1}</td>
<td>{html_options name="SKILL1" options=$skilOpt selected=$d.SKILL1}</td>
<td>{html_options name="BLANK1" options=$blankOpt selected=$d.BLANK1}</td>
</tr>

<tr>
<td><input type="TEXT" name="MEMBER2" size="15" value="{$d.MEMBER2}"></td>
<td><input type="TEXT" name="AGE2" size="2" value="{$d.AGE2}">歳</td>
<td>{html_options name="SEX2" options=$sexOpt selected=$d.SEX2}</td>
<td>{html_options name="STYLE2" options=$styleOpt selected=$d.STYLE2}</td>
<td>{html_options name="SKILL2" options=$skilOpt selected=$d.SKILL2}</td>
<td>{html_options name="BLANK2" options=$blankOpt selected=$d.BLANK2}</td>
</tr>

<tr>
<td><input type="TEXT" name="MEMBER3" size="15" value="{$d.MEMBER3}"></td>
<td><input type="TEXT" name="AGE3" size="2" value="{$d.AGE3}">歳</td>
<td>{html_options name="SEX3" options=$sexOpt selected=$d.SEX3}</td>
<td>{html_options name="STYLE3" options=$styleOpt selected=$d.STYLE3}</td>
<td>{html_options name="SKILL3" options=$skilOpt selected=$d.SKILL3}</td>
<td>{html_options name="BLANK3" options=$blankOpt selected=$d.BLANK3}</td>
</tr>

<tr>
<td><input type="TEXT" name="MEMBER4" size="15" value="{$d.MEMBER4}"></td>
<td><input type="TEXT" name="AGE4" size="2" value="{$d.AGE4}">歳</td>
<td>{html_options name="SEX4" options=$sexOpt selected=$d.SEX4}</td>
<td>{html_options name="STYLE4" options=$styleOpt selected=$d.STYLE4}</td>
<td>{html_options name="SKILL4" options=$skilOpt selected=$d.SKILL4}</td>
<td>{html_options name="BLANK4" options=$blankOpt selected=$d.BLANK4}</td>
</tr>

<tr>
<td><input type="TEXT" name="MEMBER5" size="15" value="{$d.MEMBER5}"></td>
<td><input type="TEXT" name="AGE5" size="2" value="{$d.AGE5}">歳</td>
<td>{html_options name="SEX5" options=$sexOpt selected=$d.SEX5}</td>
<td>{html_options name="STYLE5" options=$styleOpt selected=$d.STYLE5}</td>
<td>{html_options name="SKILL5" options=$skilOpt selected=$d.SKILL5}</td>
<td>{html_options name="BLANK5" options=$blankOpt selected=$d.BLANK5}</td>
</tr>

</table>
<p class="caution">ファンダイブ以外は常時開催しているわけではございませんので、<br>
日程などをお尋ねください。</p>
</td>
</tr>
<tr><th colspan="2">ダイビング希望日</th>
<td class="small1">
<p>ご存じの通り、ダイビング終了後12時間は飛行機に乗ることができませんので、<br>
帰りのスケジュールは注意してください。</p>
{html_select_date prefix="DIVING_" field_order="YMD" end_year="+1" month_format="%m月" day_format="%d日"}
から
{html_options name=DURING options=$duringOpt}
<p>到着日にダイビングをご希望の方は、石垣港発の船の時間と希望するダイビング本数をお知らせください。<br>
<input type="TEXT" name="ISGDEP" size="5" value="{$d.ISGDEP}">発
&nbsp;&nbsp;
{html_options name="TDDIVE" options=$num123Opt selected=$d.TDDIVE}本くらいを希望</p>
<p>滞在中にダイビング以外の予定が有る場合はコメント欄にご記入ください。
  (日程が決まっている場合はなるべく具体的にお願いします。)<br>
コメント欄：<input type="TEXT" name="DURINGC"  size="40"value="{$d.DURINGC}"></p>
</td></tr>
<tr><th colspan="2">西表島滞在期間</th>
<td class="small1">
{html_select_date prefix="STAYIN_" field_order="YMD" end_year="+1" month_format="%m月" day_format="%d日"}
に到着して、
{html_select_date prefix="BACKOUT_" field_order="YMD" end_year="+1" month_format="%m月" day_format="%d日"}
に出発</td></tr>

<tr><th class="B" colspan="3">その他情報</th></tr>

<tr><th colspan="2">宿の手配</th>
<td class="small1">
<p>宿泊先は、
{html_options name="HOTEL" options=$hotelOpt selected=$d.HOTEL}</p>
<p>手配済みの場合、宿の名称を記入してください。<br>
送迎を行っていない地域もございます。<br>
&nbsp;&nbsp;&nbsp;&nbsp;<input type="TEXT" name="HOTELNAME" size="44" value="{$HOTELNAME}"></p>
<p class="small1">依頼を希望される場合には、詳細な希望をご指定ください。<br>
ご希望にかなわない場合もございますのでご了承ください。</p>
<table border="0">
<tr><td>相部屋でよいですか？</td>
<td>{html_options name="ROOMSHARE" options=$yesnoOpt selected=$d.ROOMSHARE}</td></tr>
<tr><td>バス・トイレはどうされますか？</td>
<td>{html_options name="BATHTYPE" options=$bathOpt selected=$d.BATHTYPE}</td></tr>
<tr><td rowspan="3">希望の宿がありますか？<br><a href="hotel-info.html" target="HOTELINFO">宿の情報ページ</a></td>
<td>第1希望:<input type="TEXT" name="REQHOTEL1" value="{$d.REQHOTEL1}"></td></tr>
<tr><td>第2希望:<input type="TEXT" name="REQHOTEL2" value="{$d.REQHOTEL2}"></td></tr>
<tr><td>第3希望:<input type="TEXT" name="REQHOTEL3" value="{$d.REQHOTEL3}"></td></tr>
<tr><td>※予算などございましたらご記入ください。</td>
<td><input type="TEXT" name="REQHOTELC" size="20" value="{$d.REQHOTELC}"></td></tr>
</table>
</td></tr>

<tr><th colspan="2">その他情報</th>
<td class="small1">
<p>ガイドのスケジュールの参考になりますので、<br>
以下の項目もチェックしてください。<br>
なお、最後の「見たい生物」などは具体的な部分をコメントにご記入願います。</p>
{html_checkboxes name="REMARKS" separator='<br>' values=$remarks output=$remarks selected=$d.REMARKS}
<p>コメント欄：<input type="TEXT" name="REMARKSC" size="40" value="{$d.REMARKSC}"></P>
</td>
</tr>
<tr><th colspan="2">レンタル機材</th>
<td class="small1">
<p>ウェイトなどは通常準備しております。<br>
サイズの問題もありますので、必要な機材がありましたら、<br>
連絡事項に記入願います。</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;{html_options name=RENTALL options=$rentallOpt selected=$d.RENTALL}</p>
<p>コメント欄：<input type="TEXT" name="RENTALLC" size="40" value="{$d.RENTALLC}"></p>
</td></tr>
<tr><th colspan="2">連絡事項</th>
<td><textarea name="COMMENT" cols="40">
{$COMMENT}
</textarea>
</td></tr>
</table>
<input type="SUBMIT" name="GO" value="予約する">
<!-- <input type="SUBMIT" name="DEB" value="デバッグ出力"> -->
</div>
</form>
</body>
</html>
