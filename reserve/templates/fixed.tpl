<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
 <link rel="stylesheet" type="text/css" href="default.css">
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>TAKE DIVING/予約完了</title>
 <link rel="shortcut icon" href="../favicon.ico">
</head>
<body bgcolor="#66ffff">
<div align="center">
<IMG src="reserve.gif" width="268" height="135" alt="完了フォーム" border="0"></div>
<P align="center">以下のように予約を承りました。
ありがとうございます。気を付けていらっしゃってください。</p>

<HR width="100%" align="center">
<!-- <p>西表島ニラカナイへご宿泊で、ダイビングを申込んでいただこうとしているお客さまへ。</p>
<p>まず、こちらをごらんください。</p> -->

<div align="center">
<table border="0">
  <tr><th class="B" colspan="2">予約情報</th></tr>
  <tr><th>予約完了日時</th><td class="small1">{$d.TODAY}</td></tr>
  <tr><th>予約番号</th><td class="small1">{$d.RESNUM} お問い合わせの際にお伝えください。</td></tr>
  <tr><th class="B" colspan="2">申込者(代表者)連絡先</th></tr>
  <tr><th>氏名</th><td><span class="rubi">{$d.KANA}</span><br>{$d.NAME}</td></tr>
  <tr><th>性別年齢</th><td class="small1">性別：{$d.SEX}&nbsp;&nbsp;年齢：{$d.AGE|default:'？'}歳</td></tr>
  <tr><th>住所</th><td class="small1">〒{$d.ZIP} {$d.ADDR}</td></tr>
  <tr><th>E-Mail</th><td class="small1">{$d.EMAIL}</td></tr>
  <tr><th>携帯電話</th><td class="small1">{$d.CELL}</td></tr>
  <tr><th>TEL</th><td class="small1">{$d.TEL}</td></tr>
  <tr><th>FAX</th><td class="small1">{$d.FAX}</td></tr>
  <tr><th rowspan="2">利用経歴</th>
    <td class="small1">西表島でのダイビングは「{$d.IRIHIS}」</td></tr>
    <tr><td class="small1">当ショップのご利用は「{$d.REPEAT}」</td></tr>
    <tr><th class="B" colspan="2">申し込み内容</th></tr>
<tr>
  <th>参加者</th>
  <td class="small1">
{$d.PERSON}名様
<table class="small1c">
<tr>
  <th class="small1">名前</th>
  <th class="small1">年齢</th>
  <th class="small1">性別</th>
  <th class="small1">ダイビングスタイル</th>
  <th class="small1">ダイビング経験</th>
  <th class="small1">ブランク</th>
</tr>
{if $d.MEMBER1 != ""}
<tr>
<td>{$d.MEMBER1}</td>
<td>{$d.AGE1} 歳</td>
<td>{$d.SEX1}</td>
<td>{$d.STYLE1}</td>
<td>{$d.SKILL1}</td>
<td>{$d.BLANK1}</td>
</tr>
{/if}
{if $d.MEMBER2 != ""}
<tr>
<td>{$d.MEMBER2}</td>
<td>{$d.AGE2} 歳</td>
<td>{$d.SEX2}</td>
<td>{$d.STYLE2}</td>
<td>{$d.SKILL2}</td>
<td>{$d.BLANK2}</td>
</tr>
{/if}
{if $d.MEMBER3 != ""}
<tr>
<td>{$d.MEMBER3}</td>
<td>{$d.AGE3} 歳</td>
<td>{$d.SEX3}</td>
<td>{$d.STYLE3}</td>
<td>{$d.SKILL3}</td>
<td>{$d.BLANK3}</td>
</tr>
{/if}
{if $d.MEMBER4 != ""}
<tr>
<td>{$d.MEMBER4}</td>
<td>{$d.AGE4} 歳</td>
<td>{$d.SEX4}</td>
<td>{$d.STYLE4}</td>
<td>{$d.SKILL4}</td>
<td>{$d.BLANK4}</td>
</tr>
{/if}
{if $d.MEMBER5 != ""}
<tr>
<td>{$d.MEMBER5}</td>
<td>{$d.AGE5} 歳</td>
<td>{$d.SEX5}</td>
<td>{$d.STYLE5}</td>
<td>{$d.SKILL5}</td>
<td>{$d.BLANK5}</td>
</tr>
{/if}

</table>

<tr><th>ダイビング希望日</th><td class="small1">
{$d.DIVING_Year}年{$d.DIVING_Month}月{$d.DIVING_Day}日から
{$d.DURING}
{if $d.DURINGC != ""}
<br>コメント：{$d.DURINGC}
{/if}
{if $d.TDDIVE != "0"}
<br>石垣を{$d.ISGDEP}頃出発して{$d.TDDIVE}本くらい到着日に潜りたい。
{/if}
</td></tr>
<tr><th>西表島滞在期間</th>
<td class="small1">
{$d.STAYIN_Year}年{$d.STAYIN_Month}月{$d.STAYIN_Day}日に到着して、
{$d.BACKOUT_Year}年{$d.BACKOUT_Month}月{$d.BACKOUT_Day}日に出発する。
</td></tr>

<tr><th class="B" colspan="2">その他情報</th></tr>


<tr>
<th>宿の手配</th>
<td class="small1">
{if $d.HOTEL != "手配済み"}
{$d.HOTEL}<br>
相部屋は、
{if $d.ROOMSHARE == "はい"}
OK
{else}
いや
{/if}
です。<br>
バス・トイレは、{$d.BATHTYPE}
{if $d.BATHTYPE != "おまかせ"}
希望
{/if}
です。<br>
{if $d.REQHOTEL1!="" || $d.REQHOTEL2!="" || $d.REQHOTEL3!="" }
希望のホテルがあります。
<ul>
{if $d.REQHOTEL1!=""}
<li>{$d.REQHOTEL1}</li>
{/if}
{if $d.REQHOTEL2!=""}
<li>{$d.REQHOTEL2}</li>
{/if}
{if $d.REQHOTEL3!=""}
<li>{$d.REQHOTEL3}</li>
{/if}
</ul>
{/if}
{else}
{if $d.HOTELNAME != ''}
&nbsp;&nbsp;{$d.HOTELNAME} を手配済み。
{/if}
{/if}
{if $d.REQHOTELC != ''}
<br>&nbsp;&nbsp;予算などの希望：{$d.REQHOTELC}
{/if}
</td>
</tr>

<tr><th>その他情報</th>
<td class="small1">
{section name=rid loop=$d.REMARKS}
{$d.REMARKS[rid]}<br>
{/section}
{if $d.REMARKSC != ''}
<br>コメント：{$d.REMARKSC}
{/if}
</td>
</tr>
<tr>
<th rowspan="2">レンタル機材</th>
<td class="small1">{$d.RENTALL}</td>
</tr>
<tr><td class="small1">コメント：<br>
  {$d.RENTALLC}
  </td>
</tr>
<tr><th>連絡事項</th>
<td class="small1">
{$d.COMMENT}
</td></tr>
</table>
</div>
</body>
</html>
