<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
 <link rel="stylesheet" type="text/css" href="default.css">
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <title>TAKE DIVING/予約確認</title>
 <link rel="shortcut icon" href="../favicon.ico">
</head>
<body bgcolor="#66ffff">
<div align="center">
<IMG src="reserve.gif" width="268" height="135" alt="確認フォーム" border="0"></div>
<P align="center">以下のように予約を承ります。ご確認ください。<br>
OKならば、一番したの「本当に予約する」ボタンを押してください。</p>

<HR width="100%" align="center">
<!-- <p>西表島ニラカナイへご宿泊で、ダイビングを申込んでいただこうとしているお客さまへ。</p>
<p>まず、こちらをごらんください。</p> -->

<form method="POST" action="index.php" name="CONFIRM">
<input type="HIDDEN" name="NAME" value="{$d.NAME}">
<input type="HIDDEN" name="KANA" value="{$d.KANA}">
<input type="HIDDEN" name="SEX" value="{$d.SEX}">
<input type="HIDDEN" name="AGE" value="{$d.AGE}">
<input type="HIDDEN" name="ZIP" value="{$d.ZIP}">
<input type="HIDDEN" name="ADDR" value="{$d.ADDR}">
<input type="HIDDEN" name="EMAIL" value="{$d.EMAIL}">
<input type="HIDDEN" name="CELL" value="{$d.CELL}">
<input type="HIDDEN" name="TEL" value="{$d.TEL}">
<input type="HIDDEN" name="FAX" value="{$d.FAX}">
<input type="HIDDEN" name="IRIHIS" value="{$d.IRIHIS}">
<input type="HIDDEN" name="REPEAT" value="{$d.REPEAT}">
<input type="HIDDEN" name="RENTALL" value="{$d.RENTALL}">
<input type="HIDDEN" name="RENTALLC" value="{$d.RENTALLC}">
<input type="HIDDEN" name="HOTEL" value="{$d.HOTEL}">
<input type="HIDDEN" name="HOTELNAME" value="{$d.HOTELNAME}">
<input type="HIDDEN" name="ROOMSHARE" value="{$d.ROOMSHARE}">
<input type="HIDDEN" name="BATHTYPE" value="{$d.BATHTYPE}">
<input type="HIDDEN" name="REQHOTEL1" value="{$d.REQHOTEL1}">
<input type="HIDDEN" name="REQHOTEL2" value="{$d.REQHOTEL2}">
<input type="HIDDEN" name="REQHOTEL3" value="{$d.REQHOTEL3}">
<input type="HIDDEN" name="REQHOTELC" value="{$d.REQHOTELC}">
<input type="HIDDEN" name="DURING" value="{$d.DURING}">
<input type="HIDDEN" name="DURINGC" value="{$d.DURINGC}">
<input type="HIDDEN" name="TDDIVE" value="{$d.TDDIVE}">
<input type="HIDDEN" name="ISGDEP" value="{$d.ISGDEP}">
<input type="HIDDEN" name="STAYIN_Year" value="{$d.STAYIN_Year}">
<input type="HIDDEN" name="STAYIN_Month" value="{$d.STAYIN_Month}">
<input type="HIDDEN" name="STAYIN_Day" value="{$d.STAYIN_Day}">
<input type="HIDDEN" name="DIVING_Year" value="{$d.DIVING_Year}">
<input type="HIDDEN" name="DIVING_Month" value="{$d.DIVING_Month}">
<input type="HIDDEN" name="DIVING_Day" value="{$d.DIVING_Day}">
<input type="HIDDEN" name="BACKOUT_Year" value="{$d.BACKOUT_Year}">
<input type="HIDDEN" name="BACKOUT_Month" value="{$d.BACKOUT_Month}">
<input type="HIDDEN" name="BACKOUT_Day" value="{$d.BACKOUT_Day}">
<input type="HIDDEN" name="REMARKSC" value="{$d.REMARKSC}">
<input type="HIDDEN" name="COMMENT" value="{$d.COMMENT}">
<input type="HIDDEN" name="PERSON" value="{$d.PERSON}">

<div align="center">
<table border="0">
<tr><th class="B" colspan="2">申込者(代表者)連絡先</th></tr>
<tr><th>氏名</th><td><span class="rubi">{$d.KANA}</span><br>{$d.NAME}</td></tr>
<tr><th>性別年齢</th><td class="small1">性別：{$d.SEX}&nbsp;&nbsp;年齢：{$d.AGE|default:'？'}歳</td></tr>
<tr><th>住所</th><td class="small1">〒{$d.ZIP} {$d.ADDR}</td></tr>
<tr><th>E-Mail</th><td class="small1">{$d.EMAIL}</td></tr>
<tr><th>携帯電話</th><td class="small1">{$d.CELL}</td></tr>
<tr><th>TEL</th><td class="small1">{$d.TEL}</td></tr>
<tr><th>FAX</th><td class="small1">{$d.FAX}</td></tr>
<tr><th rowspan="2">利用経歴</th>
<td class="small1">西表島でのダイビングは「{$d.IRIHIS}」</td></tr>
<tr><td class="small1">当ショップのご利用は「{$d.REPEAT}」</td></tr>
<tr><th class="B" colspan="2">申し込み内容</th></tr>
<tr>
  <th>参加者</th>
  <td class="small1">
{$d.PERSON}名様
<table class="small1c">
<tr>
  <th class="small1">名前</th>
  <th class="small1">年齢</th>
  <th class="small1">性別</th>
  <th class="small1">ダイビングスタイル</th>
  <th class="small1">ダイビング経験</th>
  <th class="small1">ブランク</th>
</tr>
{if $d.MEMBER1 != ""}
<input type="HIDDEN" name="MEMBER1" value="{$d.MEMBER1}">
<input type="HIDDEN" name="AGE1" value="{$d.AGE1}">
<input type="HIDDEN" name="SEX1" value="{$d.SEX1}">
<input type="HIDDEN" name="STYLE1" value="{$d.STYLE1}">
<input type="HIDDEN" name="SKILL1" value="{$d.SKILL1}">
<input type="HIDDEN" name="BLANK1" value="{$d.BLANK1}">
<tr>
<td>{$d.MEMBER1}</td>
<td>{$d.AGE1} 歳</td>
<td>{$d.SEX1}</td>
<td>{$d.STYLE1}</td>
<td>{$d.SKILL1}</td>
<td>{$d.BLANK1}</td>
</tr>
{/if}
{if $d.MEMBER2 != ""}
<input type="HIDDEN" name="MEMBER2" value="{$d.MEMBER2}">
<input type="HIDDEN" name="AGE2" value="{$d.AGE2}">
<input type="HIDDEN" name="SEX2" value="{$d.SEX2}">
<input type="HIDDEN" name="STYLE2" value="{$d.STYLE2}">
<input type="HIDDEN" name="SKILL2" value="{$d.SKILL2}">
<input type="HIDDEN" name="BLANK2" value="{$d.BLANK2}">
<tr>
<td>{$d.MEMBER2}</td>
<td>{$d.AGE2} 歳</td>
<td>{$d.SEX2}</td>
<td>{$d.STYLE2}</td>
<td>{$d.SKILL2}</td>
<td>{$d.BLANK2}</td>
</tr>
{/if}
{if $d.MEMBER3 != ""}
<input type="HIDDEN" name="MEMBER3" value="{$d.MEMBER3}">
<input type="HIDDEN" name="AGE3" value="{$d.AGE3}">
<input type="HIDDEN" name="SEX3" value="{$d.SEX3}">
<input type="HIDDEN" name="STYLE3" value="{$d.STYLE3}">
<input type="HIDDEN" name="SKILL3" value="{$d.SKILL3}">
<input type="HIDDEN" name="BLANK3" value="{$d.BLANK3}">
<tr>
<td>{$d.MEMBER3}</td>
<td>{$d.AGE3} 歳</td>
<td>{$d.SEX3}</td>
<td>{$d.STYLE3}</td>
<td>{$d.SKILL3}</td>
<td>{$d.BLANK3}</td>
</tr>
{/if}
{if $d.MEMBER4 != ""}
<input type="HIDDEN" name="MEMBER4" value="{$d.MEMBER4}">
<input type="HIDDEN" name="AGE4" value="{$d.AGE4}">
<input type="HIDDEN" name="SEX4" value="{$d.SEX4}">
<input type="HIDDEN" name="STYLE4" value="{$d.STYLE4}">
<input type="HIDDEN" name="SKILL4" value="{$d.SKILL4}">
<input type="HIDDEN" name="BLANK4" value="{$d.BLANK4}">
<tr>
<td>{$d.MEMBER4}</td>
<td>{$d.AGE4} 歳</td>
<td>{$d.SEX4}</td>
<td>{$d.STYLE4}</td>
<td>{$d.SKILL4}</td>
<td>{$d.BLANK4}</td>
</tr>
{/if}
{if $d.MEMBER5 != ""}
<input type="HIDDEN" name="MEMBER5" value="{$d.MEMBER5}">
<input type="HIDDEN" name="AGE5" value="{$d.AGE5}">
<input type="HIDDEN" name="SEX5" value="{$d.SEX5}">
<input type="HIDDEN" name="STYLE5" value="{$d.STYLE5}">
<input type="HIDDEN" name="SKILL5" value="{$d.SKILL5}">
<input type="HIDDEN" name="BLANK5" value="{$d.BLANK5}">
<tr>
<td>{$d.MEMBER5}</td>
<td>{$d.AGE5} 歳</td>
<td>{$d.SEX5}</td>
<td>{$d.STYLE5}</td>
<td>{$d.SKILL5}</td>
<td>{$d.BLANK5}</td>
</tr>
{/if}

</table>

<tr><th>ダイビング希望日</th><td class="small1">
{$d.DIVING_Year}年{$d.DIVING_Month}月{$d.DIVING_Day}日から
{$d.DURING}
{if $d.DURINGC != ""}
<br>コメント：{$d.DURINGC}
{/if}
{if $d.TDDIVE != "0"}
<br>石垣を{$d.ISGDEP}頃出発して{$d.TDDIVE}本くらい到着日に潜りたい。
{/if}
</td></tr>
<tr><th>西表島滞在期間</th>
<td class="small1">
{$d.STAYIN_Year}年{$d.STAYIN_Month}月{$d.STAYIN_Day}日に到着して、
{$d.BACKOUT_Year}年{$d.BACKOUT_Month}月{$d.BACKOUT_Day}日に出発する。
</td></tr>

<tr><th class="B" colspan="2">その他情報</th></tr>


<tr>
<th>宿の手配</th>
<td class="small1">
{if $d.HOTEL != "手配済み"}
{$d.HOTEL}<br>
相部屋は、
{if $d.ROOMSHARE == "はい"}
OK
{else}
いや
{/if}
です。<br>
バス・トイレは、{$d.BATHTYPE}
{if $d.BATHTYPE != "おまかせ"}
希望
{/if}
です。<br>
{if $d.REQHOTEL1!="" || $d.REQHOTEL2!="" || $d.REQHOTEL3!="" }
希望のホテルがあります。
<ul>
{if $d.REQHOTEL1!=""}
<li>{$d.REQHOTEL1}</li>
{/if}
{if $d.REQHOTEL2!=""}
<li>{$d.REQHOTEL2}</li>
{/if}
{if $d.REQHOTEL3!=""}
<li>{$d.REQHOTEL3}</li>
{/if}
</ul>
{/if}
{else}
{$d.HOTEL}
{if $d.HOTELNAME != ''}
&nbsp;&nbsp;{$d.HOTELNAME}
{/if}
{/if}
{if $d.REQHOTELC != ''}
<br>&nbsp;&nbsp;予算などの希望：{$d.REQHOTELC}
{/if}
</td>
</tr>

<tr><th>その他情報</th>
<td class="small1">
{section name=rid loop=$d.REMARKS}
{$d.REMARKS[rid]}<br>
<input type="HIDDEN" name="REMARKS[]" value="{$d.REMARKS[rid]}">
{/section}
{if $d.REMARKSC != ''}
<br>コメント：{$d.REMARKSC}
p{/if}
</td>
</tr>
<tr>
<th rowspan="2">レンタル機材</th>
<td class="small1">{$d.RENTALL}</td>
</tr>
<tr><td class="small1">コメント：<br>
  {$d.RENTALLC}
  </td>
</tr>
<tr><th>連絡事項</th>
<td class="small1">
{$d.COMMENT}
</td></tr>
</table>
<input type="SUBMIT" name="GO" value="本当に予約する">
</div>
</form>
</body>
</html>
