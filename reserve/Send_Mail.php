<?php
require_once("config.php");

function Send_Mail($d) {
  $toj = mb_convert_encoding($d['NAME']." 様","JIS","UTF-8");
  $toj = mb_encode_mimeheader($toj,'ISO-2022-JP');
  $to = $toj." <".$d['EMAIL'].">";
  $sto = STAFF_TO;
  $kto = KEITAI_TO;
  $subj = utf2mime("ご予約確認メール");
  $ssubj = utf2mime("予約連絡メール");
  $froj = utf2mime("TAKEダイビング予約センター");
  $from = $froj." <autoreply@takedive.com>";
  $addh  = "From: $from\r\n";
  $addh .= "Errors-To: horimoto@holly-linux.com\r\n";
  $addh .= "Content-Type: text/plain; charset=\"iso-2022-jp\"\r\n";
  $addh .= "Content-Transfer-Encoding: 7bit\r\n";
  //
  // お客様向け
  //
  $text  = $d['NAME']."様\n";
  $text .= "西表島のTAKEダイビングスクールからダイビング予約の確認です。\n\n";
  $text .= "この度はご予約いただきありがとうございます。\n";
  $text .= "このメールにお心当たりの無い方は、 ";
  $text .= "iriomote@takedive.com ";
  $text .= "までご連絡ください。\n\n";
  $text .= Send_Mail_add_basic_info($d,false);
  $text .= Send_Mail_add_members($d,false);
  $text .= Send_Mail_add_schedule($d,false);
  $text .= Send_Mail_add_other_info($d,false);
  $text .= Send_Mail_add_signature($d,false);
  $txt  = mb_convert_encoding($text,"JIS","UTF-8");
  $ret = mail($to,$subj,$txt,$addh);    // お客様向け
  //
  // スタッフ向け
  //
  $stext = "予約が入りました。\n";
  $stext .= Send_Mail_add_basic_info($d,true);
  $stext .= Send_Mail_add_members($d,true);
  $stext .= Send_Mail_add_schedule($d,true);
  $stext .= Send_Mail_add_other_info($d,true);
  $stext .= Send_Mail_add_signature($d,true);
  $stxt  = mb_convert_encoding($stext,"JIS","UTF-8");
  $ret = mail($sto,$ssubj,$stxt,$addh); // スタッフ向け
  //
  // スタッフ携帯向け
  //
  if ($kto!="") {
    $ktext = "予約が入りました。\n";
    $ktext .= "名前：".$d['NAME']."\n";
    $ktext .= "かな：".$d['KANA']."\n";
    if ($d['CELL']!="") {
      $ktext .= "携帯：".$d['CELL']."\n";
    }
    if ($d['TEL']!="") {
      $ktext .= "電話：".$d['TEL']."\n";
    }
    $ktext .= "西表島経験：".$d['IRIHIS']."\n";
    $ktext .= "リピート歴：".$d['REPEAT']."\n";
    $ktext .= "ダイビング希望日：".$d['DIVINGDAY']."から".$d['DURING']."\n";
    $ktext .= "人数：".$d['PERSON']."名\n";
    $ktext .= "詳細は、PCのメールに届いています。\n";
    $stxt  = mb_convert_encoding($ktext,"JIS","UTF-8");
    $ret = mail($kto,$ssubj,$stxt,$addh); // スタッフ向け
  }
}
function Send_Mail_add_members($d,$c) {
  $o = "\n[参加者]\n";
  $i = 1;
  if ($d['MEMBER1']!="") {
    $o .= Send_Mail_add_members_sub($i,$d['MEMBER1'],$d['AGE1'],$d['SEX1'],$d['STYLE1'],
				    $d['SKILL1'],$d['BLANK1'],$c);
    $i++;
  }
  if ($d['MEMBER2']!="") {
    $o .= Send_Mail_add_members_sub($i,$d['MEMBER2'],$d['AGE2'],$d['SEX2'],$d['STYLE2'],
				    $d['SKILL2'],$d['BLANK2'],$c);
    $i++;
  }
  if ($d['MEMBER3']!="") {
    $o .= Send_Mail_add_members_sub($i,$d['MEMBER3'],$d['AGE3'],$d['SEX3'],$d['STYLE3'],
				    $d['SKILL3'],$d['BLANK3'],$c);
    $i++;
  }
  if ($d['MEMBER4']!="") {
    $o .= Send_Mail_add_members_sub($i,$d['MEMBER4'],$d['AGE4'],$d['SEX4'],$d['STYLE4'],
				    $d['SKILL4'],$d['BLANK4'],$c);
    $i++;
  }
  if ($d['MEMBER5']!="") {
    $o .= Send_Mail_add_members_sub($i,$d['MEMBER5'],$d['AGE5'],$d['SEX5'],$d['STYLE5'],
				    $d['SKILL5'],$d['BLANK5'],$c);
    $i++;
  }
  return($o);
}

function Send_Mail_add_members_sub($i,$d1,$d2,$d3,$d4,$d5,$d6,$c) {
  if ($c) {
    $o .= sprintf(" (%d) %-18s %2d 歳 %s %s %s %s\n",$i,$d1,$d2,$d3,$d4,$d5,$d6);
  } else {
    $o .= sprintf(" (%d) %-18s %2d 歳 %s %s\n",$i,$d1,$d2,$d3,$d4);
  }
  return($o);
}

function Send_Mail_add_basic_info($d,$c){
  $o  = "【確認内容】\n\n";
  $o .= "予約番号：".$d['RESNUM']."\n";
  $o .= "予約日時：".$d['TODAY']."\n";
  if ($c) {
    $o .= "西表島経験：「".$d['IRIHIS']."」\n";
    $o .= "リピート歴：「".$d['REPEAT']."」\n";
  }
  $o .= "お名前：".$d['NAME']." 様";
  if ($d['PERSON']>1) {
    $o .= " 御一行".$d['PERSON']."名様\n";
  } else {
    $o .= "\n";
  }
  if ($c) {
    $o .= "E-Mail：".$d['EMAIL']."\n";
  }
  $o .= "ご住所：〒".$d['ZIP']." ".$d['ADDR']."\n";
  if ($d['CELL']!="") {
    $o .= "携帯電話番号：".$d['CELL']."\n";
  }
  if ($d['TEL']!="") {
    $o .= "電話番号：".$d['TEL']."\n";
  }
  if ($d['FAX']!="") {
    $o .= "FAX：".$d['FAX']."\n";
  }
  return($o);
}

function Send_Mail_add_schedule($d,$c) {
  $o = "";
  $o .= "\n[日程]\n";
  $o .= "西表島到着出発日：".$d['INDAY']."に到着、".$d['OUTDAY']."に出発\n";
  $o .= "ダイビング希望日：".$d['DIVINGDAY']."から".$d['DURING'];
  if ($d['DURINGC'] != "") {
    $o .= " (".$d['DURINGC'].")";
  }
  $o .= "\n";
  if ($d['TDDIVE'] != "0") {
    $o .= "西表島到着日に潜りたい：石垣島".$d['ISGDEP']."発の船に乗り、".$d['TDDIVE']."本潜りたい。\n";
  }
  return($o);
}

function Send_Mail_add_other_info($d,$c) {
  $o = "\n[その他情報]\n";
  $o .= "宿の手配：".$d['HOTEL'];
  if ($d['HOTEL']!="手配済み") {
    $o .= "\n宿の希望：相部屋は、";
    if ($d['ROOMSHARE']=="はい") {
      $o .= "可能";
    } else {
      $o .= "不可";
    }
    $o .= "\n　　　　　バストイレは、";
    $o .= $d['BATHTYPE'];
    if ($d['BATHTYPE']!="おまかせ") {
      $o .= "希望";
    }
    $o .= "です。\n";
    if ($d['REQHOTEL1']!="" || $d['REQHOTEL2']!="" || $d['REQHOTEL3']!="" ) {
      $o .= "　　　　　希望のホテルは、";
      if ($d['REQHOTEL1']!="") {
	$o .= $d['REQHOTEL1']."  ";
      }
      if ($d['REQHOTEL2']!="") {
	$o .= $d['REQHOTEL2']."  ";
      }
      if ($d['REQHOTEL3']!="") {
	$o .= $d['REQHOTEL3'];
      }
      $o .= "\n";
    }
  } else {
    $o .= "　手配済みホテル名は、";
    if ($d['HOTELNAME']!="") {
      $o .= $d['HOTELNAME'];
    } else {
      $o .= "不明";
    }
    $o .= "\n";
  }
  if ($d['REQHOTELC']!="") {
    $o .= "　　　　　予算など希望は、".$d['REQHOTELC']."\n";
  }
  $o .= "レンタル機材：".$d['RENTALL']."\n";
  if ($d['RENTALLC']!="") {
    $o .= "　　　　　　　".$d['RENTALLC']."\n";
  }
  $o .= "その他情報：";
  for ($i=0;$i<count($d['REMARKS']);$i++) {
    if ($i==0) {
      $o .= $d['REMARKS'][$i]."\n";
    } else {
      $o .= "　　　　　　".$d['REMARKS'][$i]."\n";
    }
  }
  if ($d['REMAKRSC']!="") {
    $o .= "　　　　　コメント：".$d['REMARKSC']."\n";
  }
  if ($d['COMMENT']!="") {
    $o .= "連絡事項：".$d['COMMENT']."\n\n";
  }
  return($o);
}

function Send_Mail_add_signature($d,$c) {
  if ($c) {
    $o = "--------------------ここまで-------------------\n\n";
  } else {
    $o = "--\n";
    $o .= "★★★　TAKEダイビングスクール　★★★\n";
    $o .= "〒907-1541 沖縄県八重山郡西表島船浦984-55\n";
    $o .= "Tel&fax:0980-85-6871\n";
    $o .= "Mailto: iriomote@takedive.com\n\n";
    $o .= "このメールに返信しても届きませんのでご注意ください。\n\n";
  }
  return($o);
}

function utf2mime($c) {
  $t = mb_convert_encoding($c,"JIS","UTF-8");
  $o = mb_encode_mimeheader($t,'ISO-2022-JP');
  return($o);
}
?>
