#! /usr/bin/perl
#
#   郵便番号データから 郵便番号,住所　のCSVファイルを作る。
#
# iconv -f sjis -t utf-8 ken_all.csv |./zipconv.pl > zipcode.csv
#
#
while(<STDIN>) {
    s/\"//g;
    @item = split(/,/);
    if (@item[8] eq "以下に掲載がない場合") {
	@item[8]="";
    }
    printf("%s,%s%s%s\n",@item[2],@item[6],@item[7],@item[8]);
}
